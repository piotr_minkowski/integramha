package pl.nerosoft.integramha;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.jiuan.android.sdk.am.observer_comm.Interface_Observer_CommMsg_AM;
import com.jiuan.android.sdk.device.DeviceManager;
import com.jiuan.android.sdk.po.bluetooth.lpcbt.PO3Control;
import com.jiuan.android.sdk.po.observer_comm.Interface_Observer_CommMsg_PO;

public class MainActivity extends AppCompatActivity implements Interface_Observer_CommMsg_PO, Interface_Observer_CommMsg_AM {

    private DeviceManager deviceManager = DeviceManager.getInstance();
    private Handler handler = new Handler();

    private PO3Control deviceControl;

    private Button connectButton;
    private Button disconnectButton;
    private Button startButton;
    private Button stopButton;
    private Button syncButton;
    private Button endSyncButton;

    private String userId = "piotr_minkowski@wp.pl";
    private String clientId = "6552d7583287433dac25d09e23ba1250";
    private String clientSecret = "a9ca00d3fbec4746aca19e0b9291e95b";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        deviceManager.initDeviceManager(this, userId);
        deviceManager.initReceiver();
        deviceManager.initPoStateCallaback(this);

        Button b = (Button) findViewById(R.id.scanButton);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        deviceManager.scanDevice();
                        Log.i("MainActivity", "Scanning started");
                    }
                }).start();
            }
        });

        connectButton = (Button) findViewById(R.id.connectButton);
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        deviceManager.cancelScanDevice();
                        deviceControl.connect(MainActivity.this, userId, clientId, clientSecret);
                        Log.i("MainActivity", "Connecting started");
                    }
                }).start();
            }
        });

        disconnectButton = (Button) findViewById(R.id.disconnectButton);
        disconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        deviceManager.cancelScanDevice();
                        deviceControl.endRealTimeDatas();
                        deviceControl.disconnect();
                        Log.i("MainActivity", "Disconnecting started");
                    }
                }).start();
            }
        });

        startButton = (Button) findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deviceManager.cancelScanDevice();
                deviceControl.startRealTime();
                startButton.setVisibility(View.GONE);
                stopButton.setVisibility(View.VISIBLE);
                Log.i("MainActivity", "Realtime started");
            }
        });

        stopButton = (Button) findViewById(R.id.stopButton);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deviceManager.cancelScanDevice();
                deviceControl.endRealTimeDatas();
                startButton.setVisibility(View.VISIBLE);
                stopButton.setVisibility(View.GONE);
                Log.i("MainActivity", "Realtime stopped");
            }
        });

        syncButton = (Button) findViewById(R.id.syncButton);
        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deviceManager.cancelScanDevice();
                deviceControl.syncHistoryDatas();
                syncButton.setVisibility(View.GONE);
                endSyncButton.setVisibility(View.VISIBLE);
                Log.i("MainActivity", "Sync history started");
            }
        });

        endSyncButton = (Button) findViewById(R.id.endSyncButton);
        endSyncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deviceManager.cancelScanDevice();
                deviceControl.endHistoryDatas();
                syncButton.setVisibility(View.VISIBLE);
                endSyncButton.setVisibility(View.GONE);
                Log.i("MainActivity", "Sync history finished");
            }
        });
    }

    @Override
    public void msgDeviceConnect_Am(String mac, String type) {
        Log.i("MainActivity", "Connected: mac=" + mac + ", type=" + type);
    }

    @Override
    public void msgDeviceDisconnect_Am(String mac, String type) {
        Log.i("MainActivity", "Disconnected: mac=" + mac + ", type=" + type);
    }

    @Override
    public void msgDeviceConnect_Po(String mac, String type) {
        deviceManager.cancelScanDevice();
        Log.i("MainActivity", "Connected: mac=" + mac + ", type=" + type);
        deviceControl = (PO3Control) deviceManager.getPoDevice(mac);
        Log.i("MainActivity", "Control: " + deviceControl);
        deviceControl.addObserver(new MainObserver());
        deviceControl.connect(this, userId, clientId, clientSecret);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                connectButton.setVisibility(View.GONE);
                disconnectButton.setVisibility(View.VISIBLE);
                startButton.setVisibility(View.VISIBLE);
                syncButton.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void msgDeviceDisconnect_Po(String mac, String type) {
        Log.i("MainActivity", "Disconnected: mac=" + mac + ", type=" + type);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                connectButton.setVisibility(View.VISIBLE);
                disconnectButton.setVisibility(View.GONE);
                startButton.setVisibility(View.GONE);
                syncButton.setVisibility(View.GONE);
            }
        });
    }

}
