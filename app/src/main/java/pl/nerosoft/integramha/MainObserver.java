package pl.nerosoft.integramha;

import android.util.Log;

import com.jiuan.android.sdk.po.bluetooth.lpcbt.JiuanPO3Observer;

/**
 * Created by minkopi1 on 2016-01-08.
 */
public class MainObserver implements JiuanPO3Observer {

    @Override
    public void msgUserStatus(int i) {
        Log.i("MainObserver", "Status: " + i);
    }

    @Override
    public void msgBattery(int i) {
        Log.i("MainObserver", "Battery: " + i);
    }

    @Override
    public void msgHistroyData(String s) {
        Log.i("MainObserver", "History data: " + s);
    }

    @Override
    public void msgRealtimeData(String s) {
        Log.i("MainObserver", "Realtime data: " + s);
    }

    @Override
    public void msgResultData(String s) {
        Log.i("MainObserver", "Result data: " + s);
    }

}
